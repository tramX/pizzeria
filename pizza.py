__author__ = 'Jeka'

class Pizza:
    def __init__(self, price, pizza_cooking_time ):

        # Стоимость пиццы
        self.price = price

        # Время приготовления пиццы
        self.pizza_cooking_time = pizza_cooking_time

        # Состояние готовности
        self.status = False

    # Проверка готовности пиццы
    def get_pizza_readiness(self):
        self.pizza_cooking_time -= 1
        if self.pizza_cooking_time > 0:
            return False
        else:
            self.status = True
            return True

    def __str__(self):
        return '{}'.format(id(self))

    def __repr__(self):
        return '{}'.format(id(self))




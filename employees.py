__author__ = 'Jeka'
import settings
import order
import pizza

class Employees:
    ''' Клас описания сотрудников '''

    # Колличество задействованных сотрудников
    employees_count = 0

    def __init__(self, first_name, last_name, speciality, salary):
        self.first_name = first_name
        self.last_name = last_name
        Employees.employees_count += 1

        # Специальность
        self.speciality = speciality

        # Значение зароботной платы без надбавок
        self.salary = salary

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __repr__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Kitchener(Employees):
    ''' Класс описания повора '''

    def __init__(self, first_name, last_name, speciality, salary, pizza_pay):
        Employees.__init__(self, first_name, last_name, speciality, salary)

        # Прибавка за каждую испеченную пиццу
        self.pizza_pay = pizza_pay

        # Текущая пицца
        self.current_pizza = None

    # Получаем заказ на приготовление пиццы
    def setPizza(self, pizza):
        self.current_pizza = pizza

    # Вернуть результат, какая пицца готовится
    def getPizza(self):
        return self.current_pizza


class Waiter(Employees):
    ''' Класс описания официанта '''

    def __init__(self, first_name, last_name, speciality, salary, order_pay):
        Employees.__init__(self, first_name, last_name, speciality, salary)

        # Прибавка за каждый выполненый заказ
        self.order_pay = order_pay

        # Текущий заказ
        self.current_order = None

    def getCurrentOrder(self):
        return self.current_order

    # Создание заказа
    def createOrder(self, clients, pizzes):
        # Поиск свободного столика
        free_table_list = [table for table in settings.table_list if table.free == True
                           and table.getNumberOfSeats() >= clients]
        if len(free_table_list)>0:
            print('Размещяем клиента за ', free_table_list[0])

            # Указываем что выбранный столик занят
            free_table_list[0].free = False

            # Создаем список объектов пицц для заказа
            print("Создаем ", pizzes, " пицц")
            order_pizza_list = list([pizza.Pizza(20,3) for i in "#"*pizzes ])
            print(order_pizza_list)


            # Создаем заказ и обновляем данные о заказе оффицианта
            self.current_order = order.Order(order_pizza_list, free_table_list[0])

            for kitchener, pizza_to_kitchener in list(zip(list([kitchener for kitchener in settings.employees_list if kitchener.speciality =="kitchener"
                                   and kitchener.getPizza() == None]), order_pizza_list)):
                kitchener.setPizza(pizza_to_kitchener)

            return True
        else:
            print('Все столики заняты')
            return False


    # Проверка готовности заказа
    def orderReadiness(self):

        # Выбираем всех поворов, обслуживающих заказ
        for kitchener in list([kitchener for kitchener in settings.employees_list if kitchener.speciality == "kitchener" and
            kitchener.getPizza() in self.current_order.order_pizza_list]):

            # Обновляем время приготовления пиццы
            if kitchener.getPizza().get_pizza_readiness():
                kitchener.setPizza(None)

        # При готовности заказа, освобождаем оффицианта
        if self.current_order.readyOrder():
            self.current_order.table.order_is_processed(self.current_order)
            self.current_order = None





__author__ = 'Jeka'

class Order:
    def __init__(self, order_pizza_list, table):

        # Список заказанных пицц
        self.order_pizza_list = order_pizza_list

        # Столик ожидающий заказ
        self.table = table

    # Готовность заказа
    def readyOrder(self):
        if len(self.order_pizza_list) == len(list([pizza for pizza in self.order_pizza_list if pizza.status == True])):
            return True
        else:
            return False

    def getPizzaList(self):
        return self.order_pizza_list

    def __str__(self):
        return 'Количество {} столик {}'.format(self.order_pizza_list, self.table)

    def __repr__(self):
        return 'Количество {} столик {}'.format(self.order_pizza_list, self.table)

__author__ = 'Jeka'
import time
import random

from settings import employees_list, table_list


# Список клиентов ожидающих своей очереди
clients_list = []


print("Привет. Я менеджер пиццерии.")
work_day = 480
while work_day > 0:
    print("Состояние столиков")
    print("------------------")
    print("Свободные -> ", list([table.number_table for table in table_list if table.free]))
    print("Заняты -> ", list([[table.number_table, table.time_table_free] for table in table_list if table.free == False]))

    print("Оффицианты")
    print("----------")
    print("Свободные -> ", list([waiter for waiter in employees_list if waiter.speciality == "waiter" and waiter.getCurrentOrder() == None]))
    print("Заняты -> ", list([waiter for waiter in employees_list if waiter.speciality == "waiter" and waiter.getCurrentOrder()]))

    print("Повора")
    print("------")
    print("Свободные -> ", list([kitchener for kitchener in employees_list if kitchener.speciality == "kitchener" and kitchener.getPizza() == None]))
    print("Заняты -> ", list([kitchener for kitchener in employees_list if kitchener.speciality == "kitchener" and kitchener.getPizza()]))
    print("#"*100)


    if random.randrange(1,5) == random.randrange(1,5):
        clients_count = random.randrange(1,5)
        clients, pizzes = [clients_count, random.randrange(1,(clients_count+1))]
        print("Внимание новые клиенты")
        print('Колличество клиентов', clients)

        # Поиск свободных оффициантов
        free_waiter = [waiter for waiter in employees_list if waiter.speciality == "waiter" and waiter.getCurrentOrder() == None]
        if len(free_waiter) > 0:
            # Оформляем заказ
            if free_waiter[0].createOrder(clients, pizzes):
                print("Клиенты обслуживаются")
        else:
            clients_list.append([clients, pizzes])
            print("Нет свободных оффициантов ", sum(list([clients for clients, pizzes in clients_list])), " ожидают своей очереди" )


    # Проверка готовности заказов
    print("UPDATE")

    for waiter in list([waiter for waiter in employees_list if waiter.speciality == "waiter" and waiter.getCurrentOrder() != None]):
        waiter.orderReadiness()


    # Обновляем данные о столиках
    list([table.updateStatus() for table in table_list if table.free == False and table.time_table_free > 0])

    work_day -= 1
    print(work_day)
    time.sleep(1)

print("Рабочий день окончен")
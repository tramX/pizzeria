__author__ = 'Jeka'

class Table:
    """ Стол для клиентов """
    def __init__(self, number_table, number_of_seats=2):

        # Номер столика
        self.number_table = 'Number'+ str(number_table)

        # Время через которое освободится стол
        self.time_table_free = 0

        # Количество мест
        if int(number_table) < 4:
            self.number_of_seats = number_of_seats
        elif int(number_table) < 7:
            self.number_of_seats = number_of_seats+1
        else:
            self.number_of_seats = number_of_seats+2

        # Занятость столика
        self.free = True


    # Получение ожидаемого заказа
    def order_is_processed(self, order):
        # Время через которое освободится стол
        self.time_table_free = len(order.order_pizza_list)*2


    # Возвращаем колличество мест
    def getNumberOfSeats(self):
        return self.number_of_seats

    # Занят или свободен столик
    def updateStatus(self):
        if self.free:
            return True
        else:
            print ("MINIS")
            self.time_table_free -= 1
            if self.time_table_free <= 0:
                self.free = True
                return True
            else:
                return False

    def __str__(self):
        return 'Столик {} - количество мест {} - занятость {}'.format(self.number_table, self.number_of_seats, self.free)

    def __repr__(self):
        return 'Столик {} - количество мест {} - занятость {}'.format(self.number_table, self.number_of_seats, self.free)


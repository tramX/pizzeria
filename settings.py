__author__ = 'Jeka'

import hall
import employees

# Список столиков
table_list = [hall.Table(table) for table in range(1,16)]

# Список сотрудников
kitchener1 = employees.Kitchener("Николай", "Потапов", "kitchener", 200, 2)
kitchener2 = employees.Kitchener("Алексей", "Новик", "kitchener", 200, 2)
kitchener3 = employees.Kitchener("Прохор", "Репин", "kitchener", 200, 2)
kitchener4 = employees.Kitchener("Анатолий", "Иванов", "kitchener", 200, 2)

waiter1 = employees.Waiter("Иванов", "Иван", "waiter", 100, 1)
waiter2 = employees.Waiter("Петр", "Петров", "waiter", 100, 1)
waiter3 = employees.Waiter("Алексей", "Ахматов", "waiter", 100, 1)
employees_list = [kitchener1, kitchener2, kitchener3, kitchener4,
                  waiter1, waiter2, waiter3]
